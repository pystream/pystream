window.Rest =
    basepath: '/api/v0/'
    host: document.location.host

    _buildUrl: (type, resource) -> "//#{window.Rest.host}#{window.Rest.basepath}#{type}/#{resource}"

    initialFetch: ->
        for type in window.Data.TYPES
            do (type) ->
                url = window.Rest._buildUrl type, ''
                $.get(url, (ans) ->
                    for obj in ans[type+'s']
                        window.Data.handleNew type, obj
                ,'json')

    pushNew: (type, obj) ->
        url = window.Rest._buildUrl type, ''
        data = {}
        data[type] = window.Rest._clean obj
        $.ajax(url, {method: 'POST', data: JSON.stringify(data), contentType: "application/json; charset=utf-8", dataType: 'json'}).done (ans) ->
            obj.set('id', ans[type].id)

    pushDeletion: (type, obj) ->
        if window.Rest._check obj
            url = window.Rest._buildUrl type, obj.id
            $.ajax(url, {method: 'DELETE'})

    pushChange: (type, obj) ->
        if window.Rest._check obj
            url = window.Rest._buildUrl type, obj.id
            data = {}
            data[type] = window.Rest._clean obj
            $.ajax(url, {method: 'PUT', data: JSON.stringify(data), contentType: "application/json; charset=utf-8", dataType: 'json'})

    _clean: (obj) ->
        a = {}
        for k in Object.keys(obj)
            if k in ['class','container','containerProperty','ractive'] then continue
            a[k] = obj[k]
        a['type'] = obj.class['type']
        return a

    _check: (obj) ->
        res = obj.id?
        unless res
            console.warn 'REST call not applicable:', obj
        return res

window.WS =
    path: '/ws'
    host: document.location.host
    #host: '192.168.179.153:8080'

    connect: ->
        window.WS.socket = ws= new WebSocket("ws://#{window.WS.host}#{window.WS.path}")
        ws.onmessage = (rawMsg) ->
            msg = JSON.parse(rawMsg.data)
        ws.onerror = -> window.WS.onstatuschange()
        ws.onclose = -> window.WS.onstatuschange()
        ws.onopen = -> window.WS.onstatuschange()
