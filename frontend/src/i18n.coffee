I18n.fallbacks = true
I18n.defaultLocale = 'en'
I18n.locale = 'de'

window.applyI18n = ->
    $(":contains('{{')").each ->
        $(this).contents().each ->
            if this.nodeName is '#text'
                this.nodeValue = this.nodeValue.replace /{{([a-zA-Z]+)}}/g, (match, slug) -> I18n.t slug
