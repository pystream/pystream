class Configurable
    @instanceable: false
    @type: 'Configurable'
    @properties: {}
    id: null
    constructor: (@name, @containerProperty) ->
        @class = @__proto__.constructor
        @container = Ractive._ractives[@containerProperty].data[@containerProperty]
        @ractive = Ractive._ractives[@containerProperty].ractive

    tabtitle: ->
        "#{@class.type}: #{@name}"

    register: (preventPost) ->
        @container.push this
        unless preventPost
            window.Rest.pushNew @class.classType, this

    applySettings: ->
        @ractive.set "#{@containerProperty}.#{@container.indexOf this}", this
        window.Rest.pushChange @class.classType, this

    delete: ->
        @container.splice @container.indexOf(this), 1
        window.Rest.pushDeletion @class.classType, this

    set: (prop, value) -> this[prop] = value

class Input extends Configurable
    @classType: 'input'
    available: true

class Output extends Configurable
    @classType: 'output'
    available: true

window.classes = {inputs: [], outputs: []}
