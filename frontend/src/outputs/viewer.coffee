class ViewerOutput extends Output
    @instanceable: true
    @type: 'viewer'
    @properties: [
    ]

    @add: (context, name, preventRegister) ->
        vo = new ViewerOutput (if name then name else 'unknown'), 'outputs'
        unless preventRegister then vo.register()
        return vo

window.classes.outputs.push ViewerOutput
