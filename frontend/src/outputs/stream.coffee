class StreamOutput extends Output
    @instanceable: true
    @type: 'stream'
    @properties: [
        {property: 'name', type: 'text'}
        {property: 'url', type: 'text'}
    ]

    @add: (context, name, preventRegister) ->
        so = new StreamOutput (if name then name else 'unknown'), 'outputs'
        unless preventRegister then so.register()
        return so

window.classes.outputs.push StreamOutput
