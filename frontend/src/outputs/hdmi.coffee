class HDMIOutput extends Output
    @type: 'hdmi'

    tabtitle: -> "HDMI #{@name}"
    bitrate: 400

    @properties: [
        {property: 'bitrate', type: 'number'}
    ]

    @add: (context, name, preventRegister) ->
         hdmi = new HDMIOutput name, 'outputs'
         unless preventRegister then hdmi.register()
         return hdmi

window.classes.outputs.push HDMIOutput
