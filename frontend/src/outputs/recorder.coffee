class RecorderOutput extends Output
    @instanceable: true
    @type: 'recorder'
    @properties: [
        {property: 'filename', type: 'text'}
    ]

    @add: (context, name, preventRegister) ->
        ro = new RecorderOutput (if name then name else 'unknown'), 'outputs'
        unless preventRegister then ro.register()
        return ro

window.classes.outputs.push RecorderOutput
