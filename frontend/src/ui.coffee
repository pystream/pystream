safeI18n = (arg) -> if arg isnt undefined then I18n.t.apply I18n, arguments
window.I18nRactive = Ractive.extend({data:{i18n: safeI18n,log: console.log.bind(console)}})
Ractive._ractives = {}
$ -> v() for k,v of init

eventWrapper = (name, cb) ->
    (ev) ->
        console.log 'eventWrapper ', name, ':', ev.context
        if cb then cb(ev.context)
        else if ev.context[name] then ev.context[name](ev.context)
        else if ev.context.class and ev.context.class[name] then ev.context.class[name](ev.context)
        else console.log 'nothing to do here'
        return false
wrapEvents = (emitter, events) -> emitter.on(ev, eventWrapper(ev)) for ev in events

init =
    mainRactives: ->
        cont = []
        sceneData =
            scenes: cont
            class: Scene
        # init inputs and outputs
        initOne = (id) ->
            data = {instanceable: (cls for cls in classes[id] when cls.instanceable)}
            sceneData[id] = data[id] = []
            ractive = new I18nRactive({el: document.getElementById(id), template: Ractive.partials[id], data: data})
            Ractive._ractives[id] = {ractive: ractive, data: data}
            #new classes[id][0]('in', id)
            #new classes[id][1]('SpaceX', id)
            wrapEvents ractive, ['add','delete','applySettings']
            ractive.on 'add', -> $('.dropdown.open').removeClass 'open'
            return ractive
        inputR = initOne 'inputs'
        outputs = initOne 'outputs'

        #finish initializin scenes
        el = document.getElementById 'scenes'
        ractive = new I18nRactive({el: el, template: Ractive.partials.scenes, data: sceneData})
        Ractive._ractives['scenes'] = {ractive: ractive, data: sceneData}
        #Scene.add(null,'aaa')
        #Scene.add(null,'bbb')
        #Scene.add(null,'ccc')
        wrapEvents ractive, ['moveup','movedown','delete','use','drop','add','edit','applySettings']
        ractive.observe 'current.name', ->
            name = if ractive.get('current') then ractive.get('current').name else null
            if window.statusRactive then window.statusRactive.set 'currentScene', name
    api: ->
        window.WS.connect()
        window.Rest.initialFetch()
    popup: ->
        data = {show: false, content: 'foobar'}
        el = document.getElementById 'popup-container'
        ractive = new I18nRactive({el: el, template: Ractive.partials.popup, data: data})
        ractive.on 'undo', -> false
        ractive.on 'hide', -> ractive.set 'show', false
    clicks: ->
        $('body').on 'click', '[data-event]', -> $('body').trigger "click-#{$(this).attr 'data-event'}"
        $('body').on 'click-play', -> $.get('/play')
        $('body').on 'click-pause', -> $.get('/pause')
        $('body').on 'click-stop', -> $.get('/exit')
    status: ->
        data = {socket: 'connecting'}
        el = document.getElementById 'status-container'
        ractive = new I18nRactive({el: el, template: Ractive.partials.status, data: data})
        window.WS.onstatuschange = ->
            switch window.WS.socket.readyState
                when 0 then ractive.set 'socket', 'connecting'
                when 1 then ractive.set 'socket', 'open'
                when 2 then ractive.set 'socket', 'closing'
                when 3 then ractive.set 'socket', 'closed'
        window.statusRactive = ractive
