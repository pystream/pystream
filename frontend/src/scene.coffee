class Scene extends Configurable
    @type: 'scene'
    @classType: 'scene'
    @properties: [
        {property: 'name', label: 'Name', type: 'text'},
        {property: 'audiobitrate', type: 'number'},
        {property: 'videobitrate', type: 'number'},
        {property: 'resolution', type: 'select', options: [
            {value: '1920x1080'},{value: '1280x720'},
        ]},
    ]
    available: -> @name[0] isnt 'a'

    @add: (context, name, preventRegister) ->
        scene = new Scene (if name then name else 'new Scene'), 'scenes'
        unless preventRegister then scene.register()
        return scene
    moveup: -> @move true
    movedown: -> @move false
    move: (up) ->
        i = @container.indexOf this
        j = i + if up then -1 else 1
        other = @container[j]
        items = if up then [this, other] else [other, this]
        @container.splice Math.min(i,j), 2, items[0], items[1]
    delete: ->
        #TODO no current scene behaviour
        if this is @ractive.get('current') then @drop()
        if this is @ractive.get('subject') then @ractive.set 'subject', null
        super()
    use: (ctx) ->
        #TODO set previous scene.active = false and push via rest?
        @ractive.set 'current', this
        @active = true
        if ctx isnt undefined then window.Rest.pushChange @class.classType, this
    edit: -> @ractive.set 'subject', this
    applySettings: ->
        #for some reason this has to be done explicitly
        if this is @ractive.get('current') then @ractive.set 'current', this
        super()
    drop: ->
        if this is @ractive.get('current')
            @ractive.set 'current', null
            @active = false
            window.Rest.pushChange @class.classType, this
