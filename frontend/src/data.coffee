#utilize the jquery event system
window.events = $('<a>')

window.Data =
    TYPES: ['input','output','scene']

    handleNew: (type, obj) ->
        switch type
            when 'input','output'
                for clss in window.classes[type+'s']
                    if clss.type is obj.type
                        subj = clss.add null, obj.name, true
            when 'scene'
                subj = Scene.add null, obj.name, true
                if obj.active then subj.use()
        unless subj then return
        for k,v of obj
            if k is 'name' then continue
            subj.set k,v
        subj.register(true)

    handleDeletion: (type, id) -> null

    handleChange: (type, id) -> null
