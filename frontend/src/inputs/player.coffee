class PlayerInput extends Input
    @instanceable: true
    @type: 'player'
    @properties: [
        {property: 'filename', type: 'text'},
    ]

    @add: (context, name, preventRegister) ->
        pi = new PlayerInput (if name then name else 'unknown'), 'inputs'
        unless preventRegister then pi.register()
        return pi

window.classes.inputs.push PlayerInput
