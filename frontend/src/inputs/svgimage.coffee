class SVGImageInput extends Input
    @instanceable: true
    @type: 'svgimage'
    @properties: [
        {property: 'filename', type: 'text'},
    ]

    @add: (context, name, preventRegister) ->
        sii = new SVGImageInput (if name then name else 'unknown'), 'inputs'
        unless preventRegister then sii.register()
        return sii

window.classes.inputs.push SVGImageInput
