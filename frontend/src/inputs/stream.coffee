class StreamInput extends Input
    @instanceable: true
    @type: 'stream'
    @properties: [
        {property: 'name', type: 'text'},
        {property: 'transport', type: 'radio', options: [
            {value: 'rtmp'},{value: 'http'}
        ]},
        {property: 'transport2', label: 'Transport Protocol', type: 'select', options: [
            {value: 'rtmp'},{value: 'http'}
        ]},
        {property: 'flags', label: 'Flags', type: 'checkbox', options: [
            {value: 'flagA'},{value: 'flagB'},{value: 'flagC'}
        ]}
    ]
    transport: 'http'

    @add: (context, name, preventRegister) ->
        si = new StreamInput (if name then name else 'unknown'), 'inputs'
        unless preventRegister then si.register()
        return si

window.classes.inputs.push StreamInput
