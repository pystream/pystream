class HDMIInput extends Input
    @type: 'hdmi'

    tabtitle: -> "HDMI #{@name}"
    bitrate: 400

    @properties: [
        {property: 'bitrate', type: 'number'}
    ]

    @add: (context, name, preventRegister) ->
         hdmi = new HDMIInput name, 'inputs'
         unless preventRegister then hdmi.register()
         return hdmi

window.classes.inputs.push HDMIInput
