#!/bin/bash
./build.sh
while true; do
    event=$(inotifywait -r -e MODIFY *.html */ 2> /dev/null | sed 's/ MODIFY //')
    echo "$event changed, rebuilding"
    ./build.sh
done
