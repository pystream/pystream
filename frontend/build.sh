#!/bin/bash
coffee -pc <(cat src/*.coffee src/*/*.coffee) > js/src.js

echo "Ractive.partials = {" > js/templates.js
for file in $(ls templates/*.hbs); do
    name=$(basename -s .hbs $file)
    echo -n "'$name': '" >> js/templates.js
    cat $file | sed 's/$/\\/' >> js/templates.js
    echo "'," >> js/templates.js
done
echo "}" >> js/templates.js

function translations {
    for file in $(ls i18n/); do
        echo "I18n.translations.$file = {"
        cat i18n/$file
        echo "}"
    done
}
coffee -pc <(translations) >> js/translations.js
