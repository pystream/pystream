var os = require('os');
var http = require('http');
var proxy = require('redbird')({port: 8001});

var hosts = ['localhost'];
var ifaces = os.networkInterfaces();
for (var i in ifaces) {
    hosts = hosts.concat(ifaces[i].map(function (addr) {
        return addr.address;
    }));
}

var register = function (path, target) {
    for (i in hosts) {
        proxy.register(hosts[i]+path, target);
    }
}


// BEGIN

register('/api', 'http://192.168.179.153:8080/api')
register('/', 'http://localhost:8000')
