# Setup

## virtualenv

> $ sudo apt-get install python3-gi
> $ virtualenv -p python3 --system-site-packages env
> $ source env/bin/activate
> $ pip install -r REQUIREMENTS.txt

**Note!** `env/` and `REQUIREMENTS.txt` should be on the same level

### Check if virtualenv is active

> $ printenv | grep VIRTUAL_ENV

### Deactivate virtualenv

> $ deactivate

# Upgrade Packages

> $ pip freeze | cut -d = -f 1 | xargs -n 1 pip install --upgrade
