#!/usr/bin/env python3

import asyncio
import gbulb
gbulb.install()

import collections
import functools
import io
import json
import os
import signal
import threading

from datetime import date

from gi.repository import GObject
from gi.repository import Gst

import xml.etree.ElementTree as ET

def make_caps_filter(s):
    filter = Gst.ElementFactory.make('capsfilter', None)
    filter.props.caps = Gst.Caps.from_string(s)
    return filter

def link_many(pipeline, *args):
    for x in args:
        if not x.has_ancestor(pipeline):
            pipeline.add(x)
    for i in range(len(args)-1):
        args[i].link(args[i+1])

def svg_subst(svg, replacements):
    ns = {'svg': 'http://www.w3.org/2000/svg'}
    ET.register_namespace('', ns['svg'])
    ET.register_namespace('dc', 'http://purl.org/dc/elements/1.1/')
    ET.register_namespace('cc', 'http://creativecommons.org/ns#')
    ET.register_namespace('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    doc = ET.parse(io.BytesIO(svg))
    for elem in doc.findall('.//svg:text/svg:tspan', ns):
        new = replacements.get(elem.text, None)
        if new is not None:
            elem.text = new
    out = io.BytesIO()
    doc.write(out, encoding='utf-8', xml_declaration=True)
    return out.getvalue()

class SnapshotBin(Gst.Bin):
    def __init__(self):
        super(Gst.Bin, self).__init__()

        self._semaphore = threading.Semaphore()
        self._future = None
        self._lock = asyncio.Lock()

        queue = Gst.ElementFactory.make('queue', None)
        def probe(pad, info):
            if self._semaphore.acquire(blocking=False):
                return Gst.PadProbeReturn.PASS
            else:
                return Gst.PadProbeReturn.DROP
        queue.srcpads[0].add_probe(Gst.PadProbeType.BUFFER | Gst.PadProbeType.BLOCK, probe)
        self.queue = queue

        encoder = Gst.ElementFactory.make('jpegenc', None)

        sink = Gst.ElementFactory.make('appsink', None)
        sink.props.enable_last_sample = False # too few buffers in src?
        # https://bugzilla.gnome.org/show_bug.cgi?id=737427
        sink.props.emit_signals = True
        sink.props.max_buffers = 1
        sink.props.drop = False
        sink.props.sync = False
        def new_sample(sink):
            print("new-sample")
            data = sink.emit('pull-sample')
            buf = data.get_buffer()
            image = buf.extract_dup(0, buf.get_size())
            print("captured", len(image))
            def complete():
                future = self._future
                self._future = None
                if future is not None:
                    future.set_result(image)
            loop.call_soon_threadsafe(complete)
            return False
        sink.connect('new-sample', new_sample)

        link_many(self, queue, encoder, sink)

        self.add_pad(Gst.GhostPad.new("sink", queue.sinkpads[0]))

    @asyncio.coroutine
    def get(self):
        with (yield from self._lock):
            future = asyncio.Future()
            self._future = future
            self._semaphore.release()
            image = yield from asyncio.wait_for(future, timeout=None)
            print("dequeued", len(image))
            return image


class RecorderBin(Gst.Bin):
    def __init__(self, filename="/dev/null"):
        super(Gst.Bin, self).__init__()

        self._filename = filename

        queue = Gst.ElementFactory.make('queue', None)

        encoder = Gst.ElementFactory.make('x264enc', None)
        encoder.props.tune = "zerolatency"
        encoder.props.speed_preset = "veryfast"

        mux = Gst.ElementFactory.make('matroskamux', None)
        mux.props.streamable = True

        sink = Gst.ElementFactory.make('filesink', None)
        sink.props.location = filename

        link_many(self, queue, encoder, mux, sink)

        self.add_pad(Gst.GhostPad.new("sink", queue.sinkpads[0]))


class RTMPBin(Gst.Bin):
    def __init__(self, url):
        super(Gst.Bin, self).__init__()

        self._url = url

        queue = Gst.ElementFactory.make('queue', None)
        encoder = Gst.ElementFactory.make('x264enc', None)
        encoder.props.aud = True
        encoder.props.bframes = 0
        encoder.props.bitrate = 4000
        encoder.props.byte_stream = False
        encoder.props.key_int_max = 30*2
        encoder.props.speed_preset = "veryfast"
        encoder.props.tune = "zerolatency"
        parser = Gst.ElementFactory.make('h264parse', None)
        link_many(self, queue, encoder, parser)

        audiosrc = Gst.ElementFactory.make('audiotestsrc', None)
        audiosrc.set_property('is-live', True)
        audioqueue = Gst.ElementFactory.make('queue', None)
        audioencoder = Gst.ElementFactory.make('voaacenc', None)
        audioencoder.props.bitrate = 64000
        link_many(self, audiosrc, audioqueue, audioencoder)

        mux = Gst.ElementFactory.make('flvmux', None)
        mux.props.streamable = True
        self.add(mux)
        parser.link_pads(None, mux, 'video')
        audioencoder.link_pads(None, mux, 'audio')

        sink = Gst.ElementFactory.make('rtmpsink', None)
        sink.set_property('location', url)
        link_many(self, mux, sink)

        self.add_pad(Gst.GhostPad.new("sink", queue.sinkpads[0]))


class ViewerBin(Gst.Bin):
    def __init__(self):
        super(Gst.Bin, self).__init__()

        queue = Gst.ElementFactory.make('queue', None)

        sink = Gst.ElementFactory.make('xvimagesink', None)
        sink.props.sync = True

        link_many(self, queue, sink)

        self.add_pad(Gst.GhostPad.new("sink", queue.sinkpads[0]))


class CameraBin(Gst.Bin):
    def __init__(self, device="/dev/video0"):
        super(Gst.Bin, self).__init__()

        self._device = device

        src = Gst.ElementFactory.make('v4l2src', None)
        src.props.device = device

        #caps = make_caps_filter("video/x-raw, framerate=30/1")

        decoder = Gst.ElementFactory.make('jpegdec', None)

        queue = Gst.ElementFactory.make('queue', None)

        link_many(self, src, decoder, queue)

        self.add_pad(Gst.GhostPad.new("src", queue.srcpads[0]))


class PlayerBin(Gst.Bin):
    def __init__(self, filename):
        super(Gst.Bin, self).__init__()

        self._filename = filename

        src = Gst.ElementFactory.make('filesrc', None)
        src.props.location = filename

        queue = Gst.ElementFactory.make('queue', None)

        decoder = Gst.ElementFactory.make('decodebin', None)
        def pad_added(decoder, pad):
            caps = pad.query_caps(None)
            text = caps.to_string()
            print("pad-added", text)
            if text.startswith("video"):
                pad.link(queue.sinkpads[0])
        decoder.connect("pad-added", pad_added)

        link_many(self, src, decoder)
        self.add(queue)

        self.add_pad(Gst.GhostPad.new("src", queue.srcpads[0]))


class SVGImageBin(Gst.Bin):
    def __init__(self, filename):
        super(Gst.Bin, self).__init__()

        self._filename = filename

        svg = open(filename, 'rb').read()
        svg = svg_subst(svg, {
            'AUTHOR': 'Stratum 0',
            'TITLE': 'pystreamer',
            'SUBTITLE': 'stream all the things!',
            'DATE': date.today().isoformat(),
        })
        open(filename+'.tmp', 'wb').write(svg)

        src = Gst.ElementFactory.make('filesrc', None)
        src.props.location = filename+'.tmp'

        decoder = Gst.ElementFactory.make('rsvgdec', None)

        convert = Gst.ElementFactory.make('videoconvert', None)
        scale = Gst.ElementFactory.make('videoscale', None)

        caps = make_caps_filter("video/x-raw, width=1280, height=720, framerate=30/1")

        freeze = Gst.ElementFactory.make('imagefreeze', None)

        queue = Gst.ElementFactory.make('queue', None)

        link_many(self, src, decoder, convert, scale, freeze, caps, queue)

        self.add_pad(Gst.GhostPad.new("src", queue.srcpads[0]))


class Video(object):

    def __init__(self):
        self.pipeline = Gst.Pipeline()

        scene_config = SCENES[0]
        input_config = INPUTS[scene_config["input"]]
        output_configs = [OUTPUTS[x] for x in scene_config["outputs"]]

        if input_config["type"] == "svgimage":
            input_bin = SVGImageBin(input_config["filename"])
        elif input_config["type"] == "camera":
            input_bin = CameraBin()
        elif input_config["type"] == "player":
            input_bin = PlayerBin(input_config["filename"])

        videotee = Gst.ElementFactory.make('tee', None)
        link_many(self.pipeline, input_bin, videotee)

        output_bins = []
        for output_config in output_configs:
            if output_config["type"] == "viewer":
                output_bins.append(ViewerBin())
            elif output_config["type"] == "recorder":
                output_bins.append(RecorderBin(output_config["filename"]))
            elif output_config["type"] == "stream":
                output_bins.append(RTMPBin(output_config["url"]))
            link_many(self.pipeline, videotee, output_bins[-1])

        snapshot = SnapshotBin()
        self.snapshot = snapshot

        link_many(self.pipeline, videotee, snapshot)

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        bus.connect('message::stream-status', self.on_stream_status)

        self.pipeline.set_state(Gst.State.PLAYING)

    def on_error(self, bus, msg):
        print('on_error:', msg.parse_error())
        self.pipeline.set_state(Gst.State.NULL)
        #loop.stop()

    def on_eos(self, bus, msg):
        print('on_eos:', msg)
        self.pipeline.set_state(Gst.State.NULL)
        #loop.stop()

    def on_stream_status(self, bus, msg):
        obj = msg.get_stream_status_object()
        typ, owner = msg.parse_stream_status()
        parent = owner.get_parent()
        #print('on_stream_status: {} for {}@{} {}'.format(
        #    typ, owner, parent, obj))

    def kill(self):
        self.pipeline.set_state(Gst.State.NULL)
        del self.snapshot
        del self.pipeline

import aiohttp
from aiohttp import web


@asyncio.coroutine
def index(request):
    data = open('../frontend/index.html', 'rb').read()
    return web.Response(body=data)


class IndexedCollection(collections.UserDict):
    def __setitem__(self, k, v):
        assert type(k) == int
        if 'id' in v:
            assert v['id'] == k
        else:
            v['id'] = k
        self.data[k] = v

    def add(self, v):
        next = max((x['id'] for x in self.data.values())) + 1 if self.data else 0
        id = v.get('id', next)
        self[id] = v

INPUTS = IndexedCollection()
OUTPUTS = IndexedCollection()
SCENES = IndexedCollection()


def save():
    data = {
        'inputs': INPUTS.data,
        'outputs': OUTPUTS.data,
        'scenes': SCENES.data,
    }
    print(data)
    j = json.dumps(data, indent='  ', sort_keys=True)
    path = os.path.dirname(__file__)
    f = open(os.path.join(path, 'config.json'), 'w')
    f.write(j)
    f.close()

def load():
    path = os.path.dirname(__file__)
    f = open(os.path.join(path, 'config.json'), 'r')
    targets =  {
        'inputs': INPUTS,
        'outputs': OUTPUTS,
        'scenes': SCENES,
    }
    for k, v in json.load(f).items():
        for id, data in v.items():
            assert int(id) == data["id"]
            targets[k].add(data)
    f.close()

class RESTCollectionView(web.View):
    def __init__(self, *args, **kwargs):
        super(RESTCollectionView, self).__init__(*args, **kwargs)
        id = self.request.match_info.get('id')
        self.id = int(id) if id else None

    @asyncio.coroutine
    def get(self):
        if self.id is None:
            return web.json_response({
                self.topic + 's': list(self.collection.values())
            })
        else:
            return web.json_response({
                self.topic: self.collection[self.id]
            })

    @asyncio.coroutine
    def post(self):
        if self.id is None:
            data = (yield from self.request.json())[self.topic]
            self.collection.add(data)
            path = self.request.app.router[self.topic].url(
                parts={"id": data['id']})
            self.request.app.wse.send_all({
                'method': 'POST',
                'path': path,
                'data': data})
            return web.json_response({self.topic: data})
        else:
            return web.HTTPNotImplemented()

    @asyncio.coroutine
    def put(self):
        if self.id is None:
            return web.HTTPNotImplemented()
        else:
            data = (yield from self.request.json())[self.topic]
            self.collection[self.id] = data
            self.request.app.wse.send_all({
                'method': 'PUT',
                'path': self.request.path,
                'data': data})
            return web.json_response({self.topic: data})

    @asyncio.coroutine
    def delete(self):
        if self.id is None:
            return web.HTTPNotImplemented()
        else:
            del self.collection[self.id]
            self.request.app.wse.send_all({
                'method': 'DELETE',
                'path': self.request.path})
            return web.HTTPOk()


class InputRESTView(RESTCollectionView):
    topic = 'input'
    collection = INPUTS


class OutputRESTView(RESTCollectionView):
    topic = 'output'
    collection = OUTPUTS


class SceneRESTView(RESTCollectionView):
    topic = 'scene'
    collection = SCENES


class WebSocketEndpoint(object):

    def __init__(self):
        self.sockets = []

    def send_all(self, data):
        data = json.dumps(data)
        for ws in self.sockets:
            ws.send_str(data)

    @asyncio.coroutine
    def handle(self, data):
        if data['method'] == 'PAUSE':
            video.pipeline.set_state(Gst.State.PAUSED)
        elif data['method'] == 'PLAY':
            video.pipeline.set_state(Gst.State.PLAYING)

    @asyncio.coroutine
    def __call__(self, request):
        ws = web.WebSocketResponse()
        ws.start(request)

        self.sockets.append(ws)

        try:
            while True:
                msg = yield from ws.receive()
                if msg.tp == aiohttp.MsgType.text:
                    data = json.loads(msg.data)
                    yield from self.handle(data)
                elif msg.tp == aiohttp.MsgType.close:
                    print('websocket connection closed')
                    break
                elif msg.tp == aiohttp.MsgType.error:
                    print('ws connection closed with exception %s',
                          ws.exception())
                    break
        finally:
            self.sockets.remove(ws)

        return ws

@asyncio.coroutine
def pause(request):
    video.pipeline.set_state(Gst.State.PAUSED)
    return web.HTTPNoContent()

@asyncio.coroutine
def play(request):
    video.pipeline.set_state(Gst.State.PLAYING)
    return web.HTTPNoContent()

@asyncio.coroutine
def restart(request):
    global video
    video.kill()
    video = Video()
    return web.HTTPNoContent()

@asyncio.coroutine
def snapshot(request):
    image = yield from video.snapshot.get()
    return web.Response(body=image, content_type="image/jpeg")

@asyncio.coroutine
def init(loop):
    app = web.Application(loop=loop)
    app.wse = WebSocketEndpoint()
    app.router.add_route('*', '/api/v0/input/{id:\d*}', InputRESTView, name="input")
    app.router.add_route('*', '/api/v0/output/{id:\d*}', OutputRESTView, name="output")
    app.router.add_route('*', '/api/v0/scene/{id:\d*}', SceneRESTView, name="scene")
    app.router.add_route('GET', '/pause', pause)
    app.router.add_route('GET', '/play', play)
    app.router.add_route('GET', '/restart', restart)
    app.router.add_route('GET', '/snapshot', snapshot)
    app.router.add_route('GET', '/ws', app.wse)
    app.router.add_route('GET', '/', index)
    app.router.add_static('/', '../frontend')

    srv = yield from loop.create_server(app.make_handler(), '', 8080)
    print("Server started at http://127.0.0.1:8080")
    return srv


if __name__ == '__main__':
        load()
        loop = asyncio.get_event_loop()
        for signame in ('SIGINT', 'SIGTERM'):
            loop.add_signal_handler(
                getattr(signal, signame),
                loop.stop)
        Gst.init()
        video = Video()
        loop.run_until_complete(init(loop))
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            save()
            print("done")
